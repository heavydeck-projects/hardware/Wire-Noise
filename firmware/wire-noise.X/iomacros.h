/* 
 * File:   iomacros.h
 * Make access to peripherals more platform-independents.
 */

#ifndef IOMACROS_H
#define	IOMACROS_H

#include "platform-fixes.h"
#include <stdint.h>

// --- LED getter/setters ---
#define LED0_SET(X) do{ LATAbits.LATA2 = (X) ? 1 : 0; }while(0)
#define LED0_GET() (PORTAbits.RA2)

// --- Push button getters ---
#define BTN0_GET() (PORTCbits.RC0)

// --- UART TX regs ---
#define UART1_TX(X) TXREG1 = (X)
#define UART2_TX(X) TXREG2 = (X)
#define UART1_RX() (RCREG1)
#define UART2_RX() (RCREG2)
#define UART1_ERR_FRAMING() (RCSTA1bits.FERR1)
#define UART2_ERR_FRAMING() (RCSTA2bits.FERR2)

// --- Timer mactros ---


#endif	/* IOMACROS_H */
