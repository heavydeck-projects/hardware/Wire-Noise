/* 
 * File:   debug.h
 */

#ifndef DEBUG_H
#define	DEBUG_H

#define DEBUG_ENABLE

#ifdef DEBUG_ENABLE
  #define DEBUG_STAT(X) X
#else
  #define DEBUG_STAT(X)
#endif

#endif	/* DEBUG_H */

