/*
 * Let's describe how this thing works.
 * It has two serial ports, reception on one port's RX line shall result in
 * a transmission on the other port's TX line with or without errors. in addition
 * port 1 may be used for configuration after sending a LINE BREAK.
 * 
 *   PORT 1        MCU        PORT 2
 *          +---------------+
 *    ----->| RX1       TX2 |----->
 *    <-----| TX1       RX2 |<-----
 *          |               |
 *          | BTN       LED |
 *          +---------------+
 *             ^         |
 *             |         |
 *  Push btn. >+         +-> Status LED
 * 
 * A push button can be used to change the error mode by pressing and releasing
 * it, and baud rate can be reset by *holding* the button then sending the 'U'
 * (uppercase letter U; LIN SYNC byte) on PORT 1. Status LED will signal which
 * mode has been activated by flashing a number of times.
 * 
 * | Blinks | Mode                                                       |
 * +--------+------------------------------------------------------------+
 * | 1      |                                                            |
 * | 2      |                                                            |
 * | 3      |                                                            |
 * | 4      |                                                            |
 * | 5      | Baud rate reset                                            |
 */
#include "wire-noise.h"
#include "iomacros.h"

#include <stdint.h>

//Define variables declared extern on .h
volatile uint8_t noise_mode; ///<-- Error mode.
uint8_t is_config; ///<-- Working mode (config or normal).

void noise_init(void){
    noise_mode = NOISE_NONE;
    is_config = 0;
}
