/* 
 * File:   platform-fixes.h
 * This file works around any compiler-specific quirks in one place
 */

#ifndef PLATFORM_FIXES_H
#define	PLATFORM_FIXES_H

// --- Microchip's XC8 compiler ---
// --------------------------------
#ifdef __XC8__
  #include <xc.h>
  #define INTERRUPT_HI void __interrupt(high_priority) intHi
  #define INTERRUPT_LO void __interrupt(low_priority)  intLo
  #define INTERRUPT    void __interrupt() intOnly
#endif

// --- SDCC ---
// ------------
#ifdef __SDCC__
  #error Not supported just yet.
#endif

#endif	/* PLATFORM_FIXES_H */

