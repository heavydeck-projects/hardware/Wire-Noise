/*
 * File:   main.c
 * Platform-dependant code only allowed *here*
 */

//Platform and compiler includes
#include "config-16f15324.h"
#include "platform-fixes.h"
#include "iomacros.h"

//Std includes
#include <stdint.h>
#include <pic16f15324.h>

//Project includes
#include "wire-noise.h"
#include "debug.h"

//Constants
#define HOLD_PERIOD  10
#define BRGH_DEFAULT 0x03 //
#define BRGL_DEFAULT 0x3F // 9600BPS (PRESUMING 32Mhz Fosc)

static volatile uint8_t blinker_count = 0;

static void config(){
    //Everything is digital!
    ANSELA = 0x00;
    ANSELC = 0x00;
    
    //Configure IO ports
    LATA  = 0b00000000;
    TRISA = 0b00000000; //Port A, all out; Only has the LED on RA2
    
    LATC  = 0b00010100; //TX starts as high
    //Port C inputs:
    // - C0: Button
    // - C1: RX2
    // - C3: RX1
    //Outputs:
    // - C2: TX1
    // - C4: TX2
    TRISC = 0b00001011;
    
    //PPS config
    RX1DTPPS = 0x13; //RC3 <-> RX1
    RX2DTPPS = 0x11; //RC1 <-> RX2
    RC2PPS = 0x0F; // RC2 <-> TX1
    RC4PPS = 0x11; // RC4 <-> TX2
    
    //Lock PPS
    PPSLOCK = 0x55;
    PPSLOCK = 0xAA;
    PPSLOCKbits.PPSLOCKED = 1;
    
    //Timer0 (for input polling)
    T0CON1 = 0b01000000; //Using fosc.
    TMR0H = 0x06; //
    TMR0L = 0x1A; // Int every 10th of a second
    PIE0bits.TMR0IE = 1; //Enable int
    PIR0bits.TMR0IF = 0; //Clear int flag
    T0CON0 = 0b10011111; //16bit, 1/16 scaler.
    
    //Timer1 (runaway counter/RNG)
    T1CON = 0b00000001;
    T1CLK = 0b00000010;

    //Config UART
    BAUDCON1bits.BRG16 = 1; //
    BAUDCON2bits.BRG16 = 1; //16bit baudrate register
    TXSTA1 = 0b00100100; //
    TXSTA2 = 0b00100100; //Hi-speed, Async, 8-bit TX mode
    SPBRGH1 = BRGH_DEFAULT; //
    SPBRG1  = BRGL_DEFAULT; //
    SPBRGH2 = BRGH_DEFAULT; //
    SPBRG2  = BRGL_DEFAULT; //Start as a 9600bps port
    RCSTA1 = 0b00010000; //
    RCSTA2 = 0b00010000; //8-bit RX mode
    RCSTA1bits.SPEN = 1; //
    RCSTA2bits.SPEN = 1; //Enable UART
    
    //UART interrupts
    (void) UART1_RX();  //
    (void) UART2_RX();  //Clear any spurious interupt
    PIE3 = 0b10100000; //Enable all uart RX interrupts
    
    //Enable all interrupts
    INTCON = 0b11000000;
}

INTERRUPT (void){
    //Timer/Button variables (Note: All these are STATIC!!)
    static uint8_t btn0_hold_count = 0;
    static uint8_t btn0_has_hold   = 0;
    static uint8_t btn0_last       = 1; //
    static uint8_t btn0            = 1; // button state; Note these are STATIC!
    
    //Uart variables (Note: All these are STATIC!!)
    static uint8_t is_baudrate_sync = 0;
    
    // ----------------
    // --- UART1 RX ---
    // ----------------
    if(PIR3bits.RC1IF){
        if(is_baudrate_sync){
            if(BAUDCONbits.ABDOVF){
                //Overflow, return to 9600bps
                SPBRGH1 = BRGH_DEFAULT;
                SPBRG1  = BRGL_DEFAULT;
                //Blink 5 times
                blinker_count = 5 << 1;
            }
            else{
                //All OK, blink 2 times
                blinker_count = 2 << 1;
            }
            BAUDCONbits.ABDOVF = 0;
            
            //Just received a SYNC byte. make UART2 baudrate same as mine
            SPBRGH2 = SPBRGH1;
            SPBRG2  = SPBRG1;
            (void) UART1_RX();
            
            //Clear flag
            is_baudrate_sync = 0;
        }
        else{
            UART2_TX(UART1_RX());
        }
    }
    
    // ----------------
    // --- UART2 RX ---
    // ----------------
    if(PIR3bits.RC2IF){
        UART1_TX(UART2_RX());
    }
    
    // -----------------------
    // --- Timer interrupt ---
    // -----------------------
    if(PIR0bits.TMR0IF){
        //Timer 0
        PIR0bits.TMR0IF = 0;
        btn0 = BTN0_GET();
        
        //Update hold count
        if(btn0 || btn0_last){ btn0_hold_count = 0; } //If not pushed, reset counter
        else                 { btn0_hold_count++;   } //Otherwise, increment

        //Button0 release
        if(!btn0_last && btn0){
            if(btn0_has_hold){
                //This is a release-from-hold. Ignore
                btn0_has_hold = 0;
            }
            else{
                //Increment wire-noise mode
                noise_set_mode( (noise_get_mode() + 1) % NOISE_LAST );
                //Blinker = mode + 1
                blinker_count = ((noise_mode + 1) << 1);
            }
        }

        //Button0 hold
        if(btn0_hold_count >= HOLD_PERIOD){
            if(btn0_has_hold){
                //Still on hold, ignore
            }
            else{
                //Hold triggered
                btn0_has_hold = 1;

                //Blink LED 5 times
                blinker_count = 5 << 1;

                //Put Serial port 1 in Baudrate detection mode
                ///@todo Baud detection
                BAUDCON1bits.ABDEN = 1;
                is_baudrate_sync = 1;
            }
        }
        btn0_last = btn0;

        //Blink the status LED
        if(blinker_count){
            LED0_SET(!LED0_GET());
            blinker_count--;
        }
        return;
    }
    return;
}

void main(void) {
    noise_init();
    config();
    
    blinker_count = 1 << 1;
    
    do{
        ; //Do nothing of meaning on the main loop
    }while(1);
    
    return;
}
