/* 
 * File:   wire-noise.h
 */

#ifndef WIRE_NOISE_H
#define	WIRE_NOISE_H

#include <stdint.h>

// Types
enum e_noise_mode {
    NOISE_NONE,
    NOISE_BIT_FLIP,
    NOISE_BYTE_MISS,
    NOISE_BURST,
    
    NOISE_LAST //<-- Used for counting number of modes
};

//methods

///Initialize internal memory
void noise_init(void);

//pseudo-methods
#define noise_get_mode()  noise_mode
#define noise_set_mode(X) do{ noise_mode = X; }while(0)

// Status variables
extern volatile uint8_t noise_mode;

#endif	/* WIRE_NOISE_H */
